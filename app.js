


// fetch()


    // This is a method in JavaScript that is used to send request in the server


    // Syntax:
        // fetch ("urlAPI", {options/request from the user and response to the user})


// API:

// fetching all the elements in the db
let fetchPosts = ()=>{

    fetch("https://jsonplaceholder.typicode.com/posts").then(response => response.json())
    .then(data => {
        console.log(data); 
        return showPosts(data);
    })
    .catch(error=>res.send(error))


}

let specificPost = (id)=>{
    let post = {}
    if(id <= 100 && id >=1){

        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
        .then(response => response.json())
        .then(data => {return (data)}
        )

    }

}

const showPosts = (posts)=>{
    let postEntries = '';

    posts.forEach( (post) => {
        postEntries += `<div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>  
        <p id="post-body-${post.id}">${post.body}</p>
        <button onclick = "editPost('${post.id}')">Edit</button>
        <button onclick = "deletePost('${post.id}')">Delete</button>
        </div>`
    })

    document.querySelector("#div-post-entries").innerHTML = postEntries;
}


// Add Post

    document.querySelector(`#form-add-post`).addEventListener("submit", (event)=>{
        event.preventDefault();

        let title = document.querySelector("#txt-title")
        let body = document.querySelector("#txt-body")

        fetch("https://jsonplaceholder.typicode.com/posts", {
            method: "POST" , body:JSON.stringify({
                    title: title.value,
                    body: body.value,
                    userId: 1
            }),
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(response=> response.json())
        .then(data =>{
            console.log(data);
            alert("Successfully Added.")
           
            title.value = null;
            body.value = null;
        }).catch(err => console.log(err))
    })

// Edit Post

const editPost = (id)=>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	
	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
    document.querySelector('#btn-submit-update').removeAttribute('disabled');
}


const deletePost = (id)=>{
    console.log('Hi im from delete');

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: "DELETE" 
            }).then(res => res.json()).then(result=>console.log(result))

	
	document.querySelector(`#post-${id}`).remove()


}


document.querySelector("#form-edit-post").addEventListener("submit", (event)=>{
	event.preventDefault();

	let id = document.querySelector(`#txt-edit-id`).value;
    let title = document.querySelector('#txt-edit-title');
    let body = document.querySelector('#txt-edit-body');

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: "PATCH" , body:JSON.stringify({
                    title: title.value,
                    body: body.value,
            }),
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(response=> response.json())
        .then(data =>{
            console.log(data);
            alert("Successfully Updated.")

            id.value = null;
            title.value = null;
            body.value = null;

        }).catch(err => console.log(err))


	// for(let i =0; i<posts.length; i++){
	// 	if (posts[i].id.toString() === idToBeEdited){
	// 		posts[i].title = document.querySelector('#txt-edit-title').value;
	// 		posts[i].body = document.querySelector('#txt-edit-body').value;
	// 		showPosts(posts);
	// 		alert(`Edit is successful`);
	// 		break;
	// 	}
	// }
    document.querySelector('#btn-submit-update').setAttribute('disabled', true)
})



fetchPosts();
specificPost(5);